import socket
import os

SERVER_IP = "127.0.0.1"
SERVER_PORT = 9000
DATA_SIZE = 1024

class Client:
    def __init__(self, conn):
        self.conn = conn
        addr = conn.getsockname()
        print "\r Connected to server on {} port {}" . format(str(addr[0]),str(addr[1]))
    
    def worker(self):
        while True:
            response = self.recv()
            print response,
            if response[:5] == "READY":
                command = raw_input()
                self.parseCommand(command)
    
    def parseCommand(self, cmd):
        if cmd[:2] == "cd":
            self.send("CD " + cmd[3:])
            print self.recv().rstrip()
        elif cmd[:3] == "ls":
            self.send("LS")
            print self.recv().rstrip()
        elif cmd[:4] == "send":
            self.sendFile(cmd[5:])
        elif cmd[:3] == "get":
            self.recvFile(cmd[4:])
        elif cmd[:3] == "cwd":
            self.send("CWD")
            print self.recv().rstrip()
        else:
            self.send("")
            print self.recv().rstrip() 

    def recvFile(self, target):
        self.send("GET " + target)
        signal = self.recv()
        if signal[:2] != "OK":
            print "SERVER ERROR " + signal[3:]
            return
        self.send("OK")
        fp = open(target, "wb+")
        counter = 0
        receive = 0
        while True:
            data = self.recv()
            addr = self.conn.getsockname()
            if counter > 5000:
                fp.close()
                print "SERVER LOST CONNECTION"
                break

            if len(data) == 0:
               counter+1

            if data[:3] == "END":
                fp.close()
                print "Finish Receiveing {}" . format(target)
                break
            else:
                fp.write(data) 
                receive += len(data)
                print "Receiving {} from {}:{} {} byte" . format(target,str(addr[0]),str(addr[1]),receive)
                
    def sendFile(self, target):
        fp = None
        try:
            fp = open(target, "rb")
        except:
            print "FILE ERROR"
            return
        payload = fp.read()
        fp.close()
        self.send("UP "+target)
        signal = self.recv()
        if signal[:2] != "OK":
            print "SERVER ERROR"
            return
        addr = self.conn.getsockname()
        sent = 0
        for i in range((len(payload)/DATA_SIZE)+ 1):
            container = []
            if (i+1)*DATA_SIZE > len(payload):
                container = payload[i*DATA_SIZE:len(payload)]
                sent += len(container)
            else:
                container = payload[i*DATA_SIZE:(i+1)*DATA_SIZE]
                sent += 1024
            self.send(container)
            print "Sending {} to {}:{} : {} of {} " . format(target, str(addr[0]), str(addr[1]), sent ,len(payload))
        
        self.send("END")

    def send(self, payload):
        self.conn.send(payload.ljust(DATA_SIZE))

    def recv(self):
        return self.conn.recv(DATA_SIZE) 
