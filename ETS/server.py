from threading import Thread
import socket
import os

SERVER_IP = "127.0.0.1"
SERVER_PORT = 9000
DATA_SIZE = 1024

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((SERVER_IP,SERVER_PORT))
sock.listen(5)
print "\r Starting tcp listener on {} port {}" . format(SERVER_IP, SERVER_PORT)

class Server:
    def __init__(self,conn):
        self.conn = conn
        self.cwd = os.path.dirname(os.path.realpath(__file__))
    
    def send(self, payload):
        self.conn.send(payload.ljust(DATA_SIZE))
    
    def recv(self):
        return self.conn.recv(DATA_SIZE)

    def handleClient(self):
        while True:
            self.send("READY "+self.cwd)
            data = self.recv()
            addr = self.conn.getsockname()
            if len(data) == 0:
                print "Connection from "+ str(addr[0]) +":"+ str(addr[1])
                return
            else:
                self.parseProtocol(data)
    
    def parseProtocol(self, payload):
        if payload[:3] == "CWD":
            self.currentDir()
        elif payload[:2] == "CD":
            self.changeDir(payload[3:].rstrip())
        elif payload[:3] == "GET":
            self.sendFile(payload[4:].rstrip())
        elif payload[:2] == "UP":
            self.recvFile(payload[2:].rstrip())
        if payload[:2] == "LS":
            self.listDir()
        else:
            self.send("PROTOCOL NOT FOUND")

    def listDir(self):
        fls = os.listdir(self.cwd)
        fn = ""
        for fl in fls:
            fn += fl+"\n"
        self.send(fn)

    def currentDir(self):
        self.send(self.cwd)

    def changeDir(self, loc):
        old = os.getcwd()
        try:
            os.chdir(self.cwd + "/" + loc)
            self.cwd = os.getcwd()
            os.chdir(old)
            self.send("OK "+ self.cwd)
        except:
            self.send("FAIL")
    
    def sendFile(self, target):
        fp = None
        try:
            fp = open(self.cwd+"/"+target,"rb")
            self.send("OK")
        except:
            self.send("FAIL")

        payload = fp.read()
        addr = self.conn.getsockname()
        fp.close()

        while True:
            data = self.recv()
            if data[:2] == "OK":
                break
            else:
                return
        
        sent = 0
        for i in range((len(payload)/DATA_SIZE)+ 1):
            container = []
            if (i+1)*DATA_SIZE > len(payload):
                container = payload[i*DATA_SIZE:len(payload)]
                sent += len(container)
            else:
                container = payload[i*DATA_SIZE:(i+1)*DATA_SIZE]
                sent += 1024
            self.send(container)
            print "Sending {} to {}:{} : {} of {} " . format(target, str(addr[0]), str(addr[1]), sent ,len(payload))
        
        self.send("END")

    def recvFile(self, target):
        fp = open(target, "wb+")
        self.send("OK")
        counter = 0
        receive = 0
        addr = self.conn.getsockname()
        while True:
            data = self.recv()
            if counter > 5000:
                fp.close()
                print "CLIENT LOST CONNECTION"
                break

            if len(data) == 0:
               counter+1

            if data[:3] == "END":
                fp.close()
                print "Finish Receiveing {}" . format(target)
                break
            else:
                fp.write(data)
                receive += len(data)
                print "Receiving {} from {}:{} {} byte" . format(target,str(addr[0]),str(addr[1]),receive)

while True:
    print "Waiting"
    conn, addr = sock.accept()
    print 'Incoming connection from', addr
    server = Server(conn)
    thread = Thread(target=server.handleClient)
    thread.start()