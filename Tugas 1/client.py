import sys
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('127.0.0.1', 7777)
print 'Connecting to %s port %s' % server_address
sock.connect(server_address)

try:
    message = 'MANTAB JIWA'
    print 'sending "%s"' % message
    sock.sendall(message)
    
    amount_received = 0
    amount_expected = len(message)
    while amount_received < amount_expected:
        data = sock.recv(16)
        amount_received += len(data)
        print 'Received "%s"' % data
finally:
    print 'Closing socket'
    sock.close()
