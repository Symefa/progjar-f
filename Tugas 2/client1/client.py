import socket

SERVER_IP = "127.0.0.1"
SERVER_PORT = 9000

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto("HELLO", (SERVER_IP, SERVER_PORT))

def getImage():
    received = 0
    fname = ""
    while True:
        data, addr = sock.recvfrom(1024)
        if data[:4] == "NAME":
            print "\r Received filename {}" . format(str(data[5:]))
            fp = open(data[5:], "wb+")
            fname = str(data[5:])
        elif data[:4] == "DONE":
            print "\r Finish Receiving"
            fp.close()
            received = 0
        elif data[:3] == "END":
            print "\r End Connection"
            break
        else:
            fp.write(data)
            received += len(data)
            print "\r Received {} {} byte from {}" . format(fname,str(received),addr[0])

while True:
    data, addr = sock.recvfrom(1024)
    if str(data) == "BEGIN":
        print "\r Start Receiving Images"
        getImage()
    break