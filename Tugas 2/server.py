from threading import Thread
import socket
import os


SERVER_IP = "127.0.0.1"
SERVER_PORT = 9000
SIZE = 1024

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
server_address = (SERVER_IP, SERVER_PORT)
sock.bind(server_address)
print "\r Starting udp listener on {} port {}" . format(SERVER_IP, SERVER_PORT)

images = ["poros.png","poros2.png"]

def imageThread(CLIENT_IP, CLIENT_PORT):
    print "\r Thread started for {}" . format(CLIENT_IP)
    addr = (CLIENT_IP, CLIENT_PORT)
    sock.sendto("BEGIN", (addr))
    for image_name in images:
        image_size = os.stat(image_name).st_size
        sock.sendto("NAME {}" .format(image_name), (addr))

        op = open(image_name,'rb')
        image = op.read()
        block_size = 0
        for block in image:
            sock.sendto(block, (addr))
            block_size = block_size + 1
            print "\r sending {} to {}:{} : {} of {} " . format(image_name, CLIENT_IP, CLIENT_PORT, block_size ,image_size)

    sock.sendto("DONE", (addr))
    op.close()
    sock.sendto("END", (addr))



while True:
    data, addr = sock.recvfrom(SIZE) # maximum size of data
    if str(data) == "HELLO":
        thread = Thread(target=imageThread, args=(addr))
        print "\r Incoming Connection from {}" . format(addr[0])
        thread.start()
